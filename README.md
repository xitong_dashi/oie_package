# oie_package


## Name
Syntactically Rich Discriminative Training: An Effective Method for Open Information Extraction (EMNLP 2022)

## Description
We will soon publish the code for the paper mentioned above in this repository.

## Visuals
![image.png](./image.png)
![image-1.png](./image-1.png)
![image-2.png](./image-2.png)

## Installation
Coming soon

## Usage
Coming soon

